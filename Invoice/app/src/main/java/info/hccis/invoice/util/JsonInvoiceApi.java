package info.hccis.invoice.util;

import java.util.List;

import info.hccis.invoice.bo.Invoice;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface JsonInvoiceApi {

    /**
     * This abstract method to be created to allow retrofit to get list of invoices
     * @return List of invoices
     * @since 20200202
     * @author BJM (with help from the retrofit research.
     */

    @GET("invoice")
    Call<List<Invoice>> getInvoices();
    @POST("invoice")
    Call<Invoice> createInvoice(@Body Invoice invoice);

    /* not used
    @FormUrlEncoded
    @POST("invoices")
    Call<Customer> createCustomer(@Field("fullName") String fullName, @Field("address1") String address1, @Field("city") String city,
    @Field("province") String province, @Field("postalCode") String postalCode, @Field("phoneNumber") String phoneNumber, @Field("birthDate") String birthDate, @Field("loyaltyCard") String loyaltyCard);
    */

}
