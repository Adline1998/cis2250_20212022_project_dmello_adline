package info.hccis.invoice;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import info.hccis.invoice.bo.Invoice;

import info.hccis.invoice.dao.MyAppDatabase;
import info.hccis.invoice.ui.invoiceDetails.InvoiceDetailsFragment;
import info.hccis.invoice.ui.invoiceList.InvoiceListFragment;
import info.hccis.invoice.util.NotificationApplication;

public class MainActivity extends AppCompatActivity implements
        InvoiceListFragment.OnListFragmentInteractionListener,
        InvoiceDetailsFragment.OnFragmentInteractionListener
       {

private AppBarConfiguration mAppBarConfiguration;
public static MyAppDatabase myAppDatabase;
private static NavController navController = null;

           public OrderBroadcastReceiver orderBroadcastReceiver = new OrderBroadcastReceiver();

           String message = "Signed in";
           File file;
           final String FILE_NAME = "LoggedInStatus";
           private static int RC_SIGN_IN = 100;
           GoogleSignInClient mGoogleSignInClient;
@Override
protected void onCreate(Bundle savedInstanceState) {
        NotificationApplication.setContext(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        /*
         * Set onClickListener when click on fab to navigate to the invoice fragment where to add a invoice
         * Date: 15/02/2021
         * Author: adline
         * Purpose: invoice Project
         */
    GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build();
    mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    SignInButton signInButton = findViewById(R.id.sign_in_button);
    findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //snackbar used
            Snackbar.make(view, "Google Sign In", Snackbar.LENGTH_LONG).show();
            signIn();
        }
    });
    DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
        R.id.nav_home, R.id.nav_about,R.id.nav_invoice,
        R.id.nav_invoice_list, R.id.nav_tools,R.id.nav_invoice_details, R.id.nav_help,
        R.id.nav_map)
        .setDrawerLayout(drawer)
        .build();
        navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        myAppDatabase = Room.databaseBuilder(getApplicationContext(), MyAppDatabase.class, "invoicedb").allowMainThreadQueries().build();
//**************************************************************************************
    //register receiver
    //**************************************************************************************
    IntentFilter filter = new IntentFilter();
    filter.addAction("info.hccis.invoice");
    //registerReceiver(exampleReceiver, filter);
    LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
    lbm.registerReceiver(orderBroadcastReceiver, filter);
}

           @Override
           protected void onDestroy() {
               super.onDestroy();
               LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
               Log.d("BJM receiver", "unregistering");
               lbm.unregisterReceiver(orderBroadcastReceiver);

           }
           private void signIn() {
               Intent signInIntent = mGoogleSignInClient.getSignInIntent();
               startActivityForResult(signInIntent, RC_SIGN_IN);
               file = new File(this.getFilesDir(), FILE_NAME);
               FileOutputStream fileOutputStream = null;
               try {
                   fileOutputStream = new FileOutputStream(file);
               } catch (FileNotFoundException e) {
                   e.printStackTrace();
               }

               if (GoogleSignIn.getLastSignedInAccount(this) == null) {
                   try {
                       fileOutputStream.write(message.getBytes());
                       fileOutputStream.flush();
                       fileOutputStream.close();
                   } catch (IOException e) {
                       e.printStackTrace();
                   }
               }
           }
           @Override
           public void onActivityResult(int requestCode, int resultCode, Intent data) {
               super.onActivityResult(requestCode, resultCode, data);

               // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
               if (requestCode == RC_SIGN_IN) {
                   // The Task returned from this call is always completed, no need to attach
                   // a listener.
                   Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                   handleSignInResult(task);
               }
           }

           private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
               try {
                   GoogleSignInAccount account = completedTask.getResult(ApiException.class);

                   //Getting Information
                   GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
                   if (acct != null) {
                       String personName = acct.getDisplayName();
                       String personGivenName = acct.getGivenName();
                       String personFamilyName = acct.getFamilyName();
                       String personEmail = acct.getEmail();
                       String personId = acct.getId();
                       Uri personPhoto = acct.getPhotoUrl();

                       //Toast to display Information
                       Toast.makeText(this, "User email: "+personEmail, Toast.LENGTH_SHORT).show();
                       Toast.makeText(this, "Person Name: "+personName, Toast.LENGTH_SHORT).show();
                   }

                   startActivity(new Intent(this, MainActivity.class));

                   // Signed in successfully, show authenticated UI.
               } catch (ApiException e) {
                   // The ApiException status code indicates the detailed failure reason.
                   // Please refer to the GoogleSignInStatusCodes class reference for more information.
                   Log.d("signInResult:failed code=",e.toString());
               }
           }

           //Sign Out Method
           private void signOut() {
               mGoogleSignInClient.signOut()
                       .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                           @Override
                           public void onComplete(@NonNull Task<Void> task) {
                           }
                       });
           }


           public static NavController getNavController() {
        return navController;
        }
@Override
public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
        }


@Override
public boolean onOptionsItemSelected(MenuItem item) {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.onNavDestinationSelected(item, navController)
        || super.onOptionsItemSelected(item);
        }
@Override
public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
        || super.onSupportNavigateUp();
        }

           @Override
           public void onListFragmentInteraction(Invoice item) {

               Log.d("ma", "item communicated from fragment: " + item.toString());

               Bundle bundle = new Bundle();
               Gson gson = new Gson();
               bundle.putString("customer", gson.toJson(item));

        /*
          BJM 20200202
          Use the navigation controller object stored as an attribute of the main
          activity to navigate the ui to the invoice detail fragment.
        */

               getNavController().navigate(R.id.nav_invoice_details, bundle);

           }
           /**
            * This is the interaction from the details fragment.  I will want to add a new contact when
            * this button is pressed.
            *
            * @param item
            * @author BJM Modified by Adline Sandra Dmello 2022-04-21
            * @since 20200203
            */

           @Override
           public void onFragmentInteractionAddContact(Invoice item) {

               /* Code to add a contact */
               Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
               intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
               intent.putExtra(ContactsContract.Intents.Insert.NAME, item.getTutorId());
               startActivity(intent);
           }

           /**
            * This is the interaction from the details fragment.  I will want to share contact when
            * this button is pressed.
            *
            * @param item
            * @author BJM Modified by Adline Sandra Dmello 2022-04-21
            * @since 20200203
            */

           @Override
           public void onFragmentInteractionShareInfo(Invoice item) {

               Intent email = new Intent(Intent.ACTION_SEND);
               email.putExtra(Intent.EXTRA_SUBJECT, "invoice notification");
               email.putExtra(Intent.EXTRA_TEXT, item.toString());

               //need this to prompts email client only
               email.setType("message/rfc822");
               startActivity(Intent.createChooser(email, "Choose an Email invoice :"));


           }

       }