package info.hccis.invoice.ui.invoices;


import info.hccis.invoice.util.JsonInvoiceApi;
import info.hccis.invoice.util.Util;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*
 * Author: Adline Sandra Dmello
 * Date: 21:04: 2022
 * Reference: https://learntodroid.com/how-to-send-json-data-in-a-post-request-in-android/
 */
public final class InvoiceRepository {
    private static InvoiceRepository instance;

    private JsonInvoiceApi jsonInvoiceApi;

    public static InvoiceRepository getInstance() {
        if (instance == null) {
            instance = new InvoiceRepository();
        }
        return instance;
    }

    public InvoiceRepository() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Util.INVOICE_BASE_API)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonInvoiceApi = retrofit.create(JsonInvoiceApi.class);
    }

    public JsonInvoiceApi getInvoiceService() {
        return jsonInvoiceApi;
    }
}
