package info.hccis.invoice.ui.invoiceDetails;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.gson.Gson;

import info.hccis.invoice.R;
import info.hccis.invoice.bo.Invoice;
import info.hccis.invoice.ui.invoiceList.InvoiceListFragment;

public class InvoiceDetailsFragment extends Fragment {

    private InvoiceDetailsViewModel invoiceDetailsViewModel;
    private Invoice invoice;
    private TextView tvTutorID;
    private TextView tvStartDate;
    private TextView tvEndDate;
    private Button btnAddInvoice;
    private Button btnShareInfo;
    private OnFragmentInteractionListener mListener;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
          BJM 20200202
          modified by adline sandra dmello
          Note how the arguments are passed from the activity to this fragment.  The
          customer object that was chosen on the recyclerview was encoded as a json string and then
          decoded when this fragment is accessed.
         */

        if (getArguments() != null) {
            String invoiceJson = getArguments().getString("invoice");
            Gson gson = new Gson();
            invoice = gson.fromJson(invoiceJson, Invoice.class);
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        invoiceDetailsViewModel =
                new ViewModelProvider(this).get(InvoiceDetailsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_invoice_details, container, false);
        invoiceDetailsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });

        tvTutorID = root.findViewById(R.id.textViewTutorID);
        tvStartDate = root.findViewById(R.id.textViewStartDate);
        tvEndDate = root.findViewById(R.id.textViewEndDate);

        tvTutorID.setText(invoice.getTutorId());
        tvStartDate.setText(invoice.getStartDate());
        tvEndDate.setText(invoice.getEndDate());

        /*
          taken from research of last year 2021
          When the add button is clicked, will pass the camper object to the activity implementation
          method.  The MainActivity will then take care of adding the contact.
         */
        btnAddInvoice = root.findViewById(R.id.btnAddInvoice);
        btnAddInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteractionAddContact(invoice);
            }
        });

        /*
          taken from research of last year 2021
          Similar to the add contact button, to send an email will notify the MainActivity and
          let it take care of sending the email with the information passed in the camper.
         */

        btnShareInfo = root.findViewById(R.id.btnShareInfo);
        btnShareInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteractionShareInfo(invoice);
            }
        });
  return root;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof InvoiceListFragment.OnListFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteractionAddContact(Invoice item);
        void onFragmentInteractionShareInfo(Invoice invoice);
    }
}