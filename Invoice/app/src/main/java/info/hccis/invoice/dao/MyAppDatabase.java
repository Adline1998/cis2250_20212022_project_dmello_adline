package info.hccis.invoice.dao;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import info.hccis.invoice.bo.Invoice;


@Database(entities = {Invoice.class},version = 1)
public abstract class MyAppDatabase extends RoomDatabase {

    public abstract InvoiceDAO invoiceDAO();

}
