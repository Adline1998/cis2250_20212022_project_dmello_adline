package info.hccis.invoice.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import info.hccis.invoice.bo.Invoice;


@Dao
public interface InvoiceDAO {
    @Insert
    public void add(Invoice invoice);

    @Update
    public void update(Invoice invoice);

    @Delete
    public void delete(Invoice invoice);

    @Query("select * from invoice")
    public List<Invoice> get();

    @Query("select * from invoice where id=:id")
    public Invoice get(int id);

    @Query("delete from invoice")
    public void deleteAll();
}