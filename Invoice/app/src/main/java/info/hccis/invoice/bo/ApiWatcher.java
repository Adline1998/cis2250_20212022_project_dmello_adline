package info.hccis.invoice.bo;

import android.app.Activity;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import info.hccis.invoice.ui.invoiceList.InvoiceListFragment;
import info.hccis.invoice.util.JsonInvoiceApi;
import info.hccis.invoice.util.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * ApiWatcher class will be used as a background thread which will monitor the api. It will notify
 * the ui activity if the number of rows changes.
 *
 * @author BJM modified by adline 20220421
 * @since 20210329
 *
 */

public class ApiWatcher extends Thread  {

    private int lengthLastCall = -1;  //Number of rows returned

    //The activity is passed in to allow the runOnUIThread to be used.
    private Activity activity = null;

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
    @Override
    public void run() {
        super.run();
        try{
            do {

                Thread.sleep(10000); //Check api every 10 seconds

                //************************************************************************
                // A lot of this code is duplicated from the TicketOrderContent class.  It will
                // access the api and if if notes that the number of orders has changed, will
                // notify the view order fragment that the data is changed.
                //************************************************************************


                Log.d("adline","running");

                //Use Retrofit to connect to the service
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Util.INVOICE_BASE_API)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                JsonInvoiceApi jsonInvoiceApi = retrofit.create(JsonInvoiceApi.class);

                //Create a list of invoices
                Call<List<Invoice>> call = jsonInvoiceApi.getInvoices();


                call.enqueue(new Callback<List<Invoice>>() {

                @Override
                public void onResponse(Call<List<Invoice>> call, Response<List<Invoice>> response) {

                    List<Invoice> invoices = new ArrayList();

                    if (!response.isSuccessful()) {
                        Log.d("ad", "not successful response from rest for invoice Code=" + response.code());

                    } else {

                        invoices = response.body();
                        int lengthThisCall = invoices.size();

                        if (lengthLastCall == -1) {
                            //first time - don't notify
                            lengthLastCall = lengthThisCall;
                        } else if (lengthThisCall != lengthLastCall) {
                            //data has changed
                            Log.d("ad", "Data has changed");
                            lengthLastCall = lengthThisCall;

                            //**********************************************************************
                            // This method will allow a call to the runOnUiThread which will be allowed
                            // to interact with the ui components of the app.
                            //**********************************************************************
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    InvoiceListFragment.notifyDataChanged("Update - the data has changed");
                                }

                            });

                        } else {
                            Log.d("adline", "Data has not changed");
                        }
                    }
                }

                    @Override
                    public void onFailure(Call<List<Invoice>> call, Throwable t) {

                        //**********************************************************************************
                        // If the api call failed, give a notification to the user.
                        //**********************************************************************************
                        Log.d("adline", "api call failed");
                        Log.d("adline", t.getMessage());
                    }
                });

            } while (true);
        } catch (InterruptedException e) {
            Log.d("adline","Thread interrupted.  Stopping in the thread.");
        }
    }
}

