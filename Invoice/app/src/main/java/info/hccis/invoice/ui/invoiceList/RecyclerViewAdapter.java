package info.hccis.invoice.ui.invoiceList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import info.hccis.invoice.R;
import info.hccis.invoice.bo.Invoice;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{

    private final List<Invoice> mValues;
    private final InvoiceListFragment.OnListFragmentInteractionListener mListener;
    //private List<Customer> filteredCustomerList;

    public RecyclerViewAdapter(List<Invoice> items, InvoiceListFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
        //filteredCustomerList = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_invoice_row, parent, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position){
        holder.mItem = mValues.get(position);
        holder.textViewTutorID.setText(" "+mValues.get(position).getTutorId());
        holder.textViewStartDate.setText(" "+mValues.get(position).getStartDate());
        holder.textViewEndDate.setText(" "+mValues.get(position).getEndDate());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });

    }
    @Override
    public int getItemCount() {
        return mValues.size();
    }

public class ViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    public final TextView textViewTutorID;
    public final TextView textViewStartDate;
    public final TextView textViewEndDate;

    public Invoice mItem;

    public ViewHolder(View view) {
        super(view);
        mView = view;
        textViewTutorID = (TextView) view.findViewById(R.id.textViewTutorIDListItem);
        textViewStartDate = (TextView) view.findViewById(R.id.textViewStartDateListItem);
        textViewEndDate = (TextView) view.findViewById(R.id.textViewEndDateListItem);
    }

    @Override
    public String toString() {
        return super.toString();
    }
 }
}

