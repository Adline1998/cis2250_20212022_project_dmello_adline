package info.hccis.invoice.util;

import android.app.NotificationManager;
import android.content.Context;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import info.hccis.invoice.R;

public class NotificationUtil {

    public static synchronized void sendNotification(String title, String message) {
        //Channel Id is ignored on lower APIs
        Log.d("adline notification", "Sending a notification");
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(NotificationApplication.getContext(), NotificationApplication.MEMBER_CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_send)
                        .setContentTitle(title)
                        .setContentText(message);

        NotificationManager notificationManager = (NotificationManager) NotificationApplication.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, builder.build());
    }
}
