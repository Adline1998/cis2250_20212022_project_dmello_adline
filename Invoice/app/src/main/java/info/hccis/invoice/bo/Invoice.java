package info.hccis.invoice.bo;


import android.widget.EditText;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

import info.hccis.invoice.util.Util;

@Entity(tableName = "invoice")
public class Invoice implements Serializable {
    public static final String INVOICE_BASE_API = Util.BASE_SERVER+"/api/InvoiceService/";
    private static final long serialVersionUID = 1L;
    //Note these match the field names from the database (and keys in json)
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("tutorId")
    @ColumnInfo(name = "tutorId")
    private int tutorId;

    @SerializedName("startDate")
    @ColumnInfo(name = "startDate")
    private String startDate;

    @SerializedName("endDate")
    @ColumnInfo(name = "endDate")
    private String endDate;

    public Invoice() {

    }

    public Invoice(int id) {
        this.id = id;
    }

    public Invoice(int tutorId,String startDate, String endDate) {

        this.tutorId = tutorId;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;

    }

    public int getTutorId() {
        return tutorId;
    }

    public void setTutorId(int tutorId) {
        this.tutorId = tutorId;
    }

    public String getStartDate() {
        return startDate;
    }


    public String getEndDate() {
        return endDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Invoice Details" + System.lineSeparator()
                + "Id =  " + id
                + " Tutor Id = " + tutorId +
                ", Start Date = " + startDate +
                ", end Date = " + endDate;
    }
}