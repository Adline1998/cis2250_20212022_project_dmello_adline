package info.hccis.invoice.ui.invoiceDetails;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

import info.hccis.invoice.bo.Invoice;

public class InvoiceDetailsViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public InvoiceDetailsViewModel() {
        mText = new MutableLiveData<>();
        //mText.setValue("")
    }
    public LiveData<String> getText() {
        return mText;
    }
}