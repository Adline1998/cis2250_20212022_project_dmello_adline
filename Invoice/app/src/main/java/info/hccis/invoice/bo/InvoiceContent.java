package info.hccis.invoice.bo;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import info.hccis.invoice.MainActivity;
import info.hccis.invoice.R;
import info.hccis.invoice.ui.invoiceList.InvoiceListFragment;
import info.hccis.invoice.util.JsonInvoiceApi;
import info.hccis.invoice.util.NotificationUtil;
import info.hccis.invoice.util.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class InvoiceContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<Invoice> INVOICES = new ArrayList<Invoice>();

    public static void reloadInvoicesInRoom(List<Invoice> invoices)
    {
        MainActivity.myAppDatabase.invoiceDAO().deleteAll();
        for(Invoice current : invoices)
        {
            MainActivity.myAppDatabase.invoiceDAO().add(current);
        }
        Log.d("ad","loading invoices from Room");
    }


    public static List<Invoice> getInvoicesFromRoom()
    {
        Log.d("ad","Loading invoices from Room");

        List<Invoice> invoiceBack = MainActivity.myAppDatabase.invoiceDAO().get();
        Log.d("ad","Number of invoices loaded: " + invoiceBack.size());
        for(Invoice current : invoiceBack)
        {
            Log.d("ad",current.toString());
        }
        return invoiceBack;
    }

    /**
     * Load the invoices.  This method will use the rest service to provide the data.
     * This is the list which is used to back the RecyclerView.
     *
     */

    public static void loadInvoices(Activity context) {

        Log.d("ad", "Accessing api at:" + Util.INVOICE_BASE_API);

        //Use Retrofit to connect to the service
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Util.INVOICE_BASE_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonInvoiceApi jsonInvoiceApi = retrofit.create(JsonInvoiceApi.class);

        //Create a list of invoices.
        Call<List<Invoice>> call = jsonInvoiceApi.getInvoices();

        //final reference to activity to get shared preferences
        final Activity contextIn = context;

        call.enqueue(new Callback<List<Invoice>>() {

            @Override
            public void onResponse(Call<List<Invoice>> call, Response<List<Invoice>> response) {

                List<Invoice> invoices;

                if(!response.isSuccessful())
                {
                    Log.d("AD-Rest","Unsuccessful response from rest: " + response.code());
                    SharedPreferences sharedPref = contextIn.getPreferences(Context.MODE_PRIVATE);
                    boolean preferToLoad = sharedPref.getBoolean(contextIn.getString(R.string.preference_load_from_room),false);
                    if(preferToLoad)
                    {
                        invoices = getInvoicesFromRoom();
                        NotificationUtil.sendNotification("Invoices loaded", "(" + invoices.size() + ") Invoices loaded from Room");
                    } else {
                        NotificationUtil.sendNotification("Invoices not loaded", "Cannot load Invoices - Connection Error");
                        return;
                    }
                } else {
                    invoices = response.body();
                    NotificationUtil.sendNotification("Invoices loaded", "(" + invoices.size() + ") invoices loaded from API service");

                    //Have successfully connected.  Reload the tutor database.
                    reloadInvoicesInRoom(invoices);

                }

                Log.d("ad", "data back from service call #returned=" + invoices.size());

                //**********************************************************************************
                // Now that we have the customers, will use them to assign values to the list which
                // is backing the recycler view.
                //**********************************************************************************

                InvoiceContent.INVOICES.clear();
                InvoiceContent.INVOICES.addAll(invoices);

                //**********************************************************************************
                // The CustomerListFragment has a recyclerview which is used to show the customer list.
                // This recyclerview is backed by the customer list in the CustomerContent class.  After
                // this list is loaded, need to notify the adapter from the recyclerview that the
                // data is changed.
                //**********************************************************************************

                InvoiceListFragment.getRecyclerView().getAdapter().notifyDataSetChanged();
                //Remove the progress bar from the view.
                InvoiceListFragment.clearProgressBarVisibility();

            }

            @Override
            public void onFailure(Call<List<Invoice>> call, Throwable t) {
                // If the api call failed, give a notification to the user.
                Log.d("adline- ", "api call failed");
                Log.d("adline ", t.getMessage());

                SharedPreferences sharedPref = contextIn.getPreferences(Context.MODE_PRIVATE);

                boolean preferToLoad = sharedPref.getBoolean(contextIn.getString(R.string.preference_load_from_room),false);

                InvoiceContent.INVOICES.clear();
                if (preferToLoad) {
                    InvoiceContent.INVOICES.addAll(getInvoicesFromRoom());
                    //Remove the progress bar from the view.
                    NotificationUtil.sendNotification("Invoices loaded", "(" + InvoiceContent.INVOICES.size() + ") Invoices loaded from room");
                } else {
                    InvoiceContent.INVOICES.clear();
                    //Remove the progress bar from the view.
                    NotificationUtil.sendNotification("Customers not loaded", "Cannot load Customers - Connection Error");
                }
                InvoiceListFragment.clearProgressBarVisibility();
                InvoiceListFragment.getRecyclerView().getAdapter().notifyDataSetChanged();
            }
        });
    }
}