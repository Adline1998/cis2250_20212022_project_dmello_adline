package info.hccis.invoice.ui.invoices;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import info.hccis.invoice.R;
import info.hccis.invoice.bo.Invoice;
import info.hccis.invoice.databinding.FragmentInvoiceBinding;
import info.hccis.invoice.util.ContentProviderUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvoiceFragment extends Fragment {

    private InvoiceViewModel invoiceViewModel;
    private EditText editTextTutorID;
    private EditText editTextStartDate;
    private EditText editTextEndDate;

    private Button buttonSubmit;
    final Calendar myCalendar= Calendar.getInstance();

    private Invoice invoices;

    private InvoiceRepository invoiceRepository;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        invoiceViewModel =
                new ViewModelProvider(this).get(InvoiceViewModel.class);
        View root = inflater.inflate(R.layout.fragment_invoice, container, false);
        //final TextView textView = root.findViewById(R.id.text_home);
        invoiceViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }

        });

        editTextTutorID = root.findViewById(R.id.editTextTutorID);
        editTextStartDate = root.findViewById(R.id.editTextstartDate);
        editTextEndDate = root.findViewById(R.id.editTextendDate);

        invoiceRepository = invoiceRepository.getInstance();

        DatePickerDialog.OnDateSetListener sDate =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH,month);
                myCalendar.set(Calendar.DAY_OF_MONTH,day);
                updateStartDate();
            }
        };

        editTextStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getActivity(),sDate,myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        DatePickerDialog.OnDateSetListener eDate =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH,month);
                myCalendar.set(Calendar.DAY_OF_MONTH,day);
                updateEndDate();
            }
        };

        editTextEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getActivity(),eDate,myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        //submit button
        buttonSubmit = root.findViewById(R.id.buttonSave);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Adline-MainActivity", "tutor ID entered =" + editTextTutorID.getText().toString());
                Log.d("Adline-MainActivity", "startDate = " + editTextStartDate.getText().toString());
                Log.d("Adline-MainActivity", "endDate = " + editTextEndDate.getText().toString());
                Log.d("Adline-MainActivity", "Save button was clicked.");


                int id =0;

                int tutorId =0;
                try {
                    tutorId = Integer.parseInt(editTextTutorID.getText().toString());
                }catch(Exception e){
                    tutorId = 0;
                  Toast.makeText(getContext(), "Tutor cannot be blank and should be an integer", Toast.LENGTH_SHORT).show();
                }
                String startDate ="";
                startDate = editTextStartDate.getText().toString();

                String endDate ="";
                endDate = editTextEndDate.getText().toString();

                Invoice i = new Invoice(tutorId,startDate,endDate);
                final int callbackId = 42;
                ContentProviderUtil.checkPermission(getActivity(), callbackId, Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR);


                //invoice repository updation to return all the invoices
                invoiceRepository.getInvoiceService().createInvoice(i).enqueue(new Callback<Invoice>() {
                    @Override
                    public void onResponse(Call<Invoice> call, Response<Invoice> r) {

                        //toast implemented
                        Toast.makeText(getContext(), "Invoice  is successfully added!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<Invoice> call, Throwable t) {
                        Toast.makeText(getContext(), "Error Adding Invoice: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

                long eventID = ContentProviderUtil.createEvent(getActivity(), invoiceRepository.toString());
                Toast.makeText(getActivity(), "Calendar Event Created (" + eventID + ")", Toast.LENGTH_SHORT);
                Log.d("Adline Calendar", "Calendar Event Created (" + eventID + ")");

                //Send a broadcast.
                LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(view.getContext());
                Intent intent = new Intent();
                intent.setAction("info.hccis.invoice");
                lbm.sendBroadcast(intent);
                getActivity().sendBroadcast(intent);

                return;
            }

        });

        return root;
    }
    private void updateStartDate() {
        String myFormat="dd-MM-yyyy";
        SimpleDateFormat dateFormat=new SimpleDateFormat(myFormat, Locale.US);
        editTextStartDate.setText(dateFormat.format(myCalendar.getTime()));
    }

    private void updateEndDate() {
        String myFormat="dd-MM-yyyy";
        SimpleDateFormat dateFormat=new SimpleDateFormat(myFormat, Locale.US);
        editTextEndDate.setText(dateFormat.format(myCalendar.getTime()));
    }

}