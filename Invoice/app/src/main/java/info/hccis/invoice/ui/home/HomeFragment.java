package info.hccis.invoice.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import info.hccis.invoice.R;
import info.hccis.invoice.databinding.FragmentHomeBinding;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private ViewPager viewPager;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        //final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }

        });
        viewPager = (ViewPager) root.findViewById(R.id.viewPager);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getContext());
        viewPager.setAdapter(viewPagerAdapter);

        /*
         * Set on click listener when click on submit in Home fragment to check for empty first and last name
         * Date: 2022/03/23
         * Purpose: used for material design presentation - display an alert dialog
         */
//        Button button = root.findViewById(R.id.btnSubmit);
//        EditText editTextViewFirstName = root.findViewById(R.id.editTextTextCamperFirstName);
//        EditText editTextViewLastName = root.findViewById(R.id.editTextTextCamperLastName);
//
//        button.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View view) {
//                if(editTextViewFirstName.getText().toString().isEmpty() || editTextViewLastName.getText().toString().isEmpty()){
//                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
//                    // Setting Dialog Title
//                    alertDialog.setTitle("ERROR");
//                    // Setting Dialog Message
//                    alertDialog.setMessage("Missing information ...");
//                    // Setting Icon to Dialog
//                    alertDialog.setIcon(R.drawable.ic_error);
//                    // Setting "Submit" Btn
//                    alertDialog.setNeutralButton("OK",
//                            new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    dialog.cancel();
//                                }
//                            });
//                    alertDialog.show();
//                } else {
//                    Toast toast = Toast.makeText(getContext(),  editTextViewFirstName.getText().toString() +" "+ editTextViewLastName.getText().toString()+ " is added",Toast.LENGTH_LONG);
//                    toast.setGravity(Gravity.TOP,0,250);
//                    toast.show();
//                }
//            }
//        });

        return root;
    }
}