package info.hccis.invoice.api;

import android.content.SharedPreferences;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import info.hccis.invoice.MainActivity;
import info.hccis.invoice.R;
import info.hccis.invoice.ViewInvoiceFragment;
import info.hccis.invoice.entity.InvoiceDetailViewModel;
import info.hccis.invoice.entity.InvoiceDetails;
import info.hccis.invoice.entity.InvoiceDetailsContent;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiWatcher extends Thread {

    public static final String BASE_SERVER = "http://10.0.2.2:8083";   //Local host
    public static final String INVOICE_BASE_API = BASE_SERVER + "/api/InvoiceService/";


    private int lengthLastCall = -1;  //Number of rows returned

    //The activity is passed in to allow the runOnUIThread to be used.
    private FragmentActivity activity = null;

    public void setActivity(FragmentActivity activity) {
        this.activity = activity;
    }

    @Override
    public void run() {
        super.run();
        try {
            do {

                Thread.sleep(10000); //Check api every 10 seconds

                //************************************************************************
                // A lot of this code is duplicated from the TicketOrderContent class.  It will
                // access the api and if if notes that the number of orders has changed, will
                // notify the view order fragment that the data is changed.
                //************************************************************************


                Log.d("adline- api", "running");

                //Use Retrofit to connect to the service
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ApiWatcher.INVOICE_BASE_API)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                JsonInvoiceDetailsApi jsonInvoiceDetailsApi = retrofit.create(JsonInvoiceDetailsApi.class);

                //Create a list of ticket orders.
                Call<List<InvoiceDetails>> call = jsonInvoiceDetailsApi.getInvoices();

                InvoiceDetailViewModel invoiceDetailViewModel = new ViewModelProvider(activity).get(InvoiceDetailViewModel.class);
                call.enqueue(new Callback<List<InvoiceDetails>>() {

                    @Override
                    public void onResponse(Call<List<InvoiceDetails>> call, Response<List<InvoiceDetails>> response) {

                        //See if we can get the view model.  This contains the list of orders
                        //which is used to populate the recycler view on the list fragment.
                        Log.d("Adline api", "found invoice details view model. size=" + invoiceDetailViewModel.getInvoices().size());


                        if (!response.isSuccessful()) {
                            Log.d("Adline api", "Adline not successful response from rest for invoice details Code=" + response.code());

                            loadFromRoomIfPreferred(invoiceDetailViewModel);
                            lengthLastCall = -1;

                        } else {
                            //Take the list and pass to constructor of ArrayList to convert it.
                            ArrayList<InvoiceDetails> invoiceDetailsTemp = new ArrayList(response.body()); //note gson will be used implicitly
                            int lengthThisCall = invoiceDetailsTemp.size();

                            Log.d("Adline api", "back from api, size=" + lengthThisCall);

                            if (lengthLastCall == -1) {
                                //first time - don't notify
                                lengthLastCall = lengthThisCall;
                                //******************************************************************
                                //Note here.  The recycler view is using the ArrayList from this
                                //ticketOrderViewModel class.  We will take the ticket orders obtained
                                //from the api and add refresh the list .  Will then notify the
                                //recyclerview that things have changed.
                                //******************************************************************
                                invoiceDetailViewModel.setInvoices(invoiceDetailsTemp); //Will addAll
                                Log.d("Adline api ", "First load of invoice details from the api");

                                //**********************************************************************
                                // This method will allow a call to the runOnUiThread which will be allowed
                                // to interact with the ui components of the app.
                                //**********************************************************************
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.d("Adline api", "trying to notify adapter that things have changed");
                                        Log.d("adline api", "Also using method to send a notification to the user");
                                        ViewInvoiceFragment.notifyDataChanged("Update - the data has changed", activity, MainActivity.class);

                                    }
                                });

                                //******************************************************************
                                //Save latest orders in the database.
                                //******************************************************************
                                InvoiceDetailsContent.reloadInvoiceDetailsInRoom(invoiceDetailsTemp);




                            } else if (lengthThisCall != lengthLastCall) {
                                //******************************************************************
                                //data has changed
                                //******************************************************************
                                Log.d("Adline api", "Data has changed");
                                lengthLastCall = lengthThisCall;
                                invoiceDetailViewModel.getInvoices().clear();
                                invoiceDetailViewModel.getInvoices().addAll(invoiceDetailsTemp);

                                //**********************************************************************
                                // This method will allow a call to the runOnUiThread which will be allowed
                                // to interact with the ui components of the app.
                                //**********************************************************************
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.d("Adline api", "trying to notify adapter that things have changed");
                                        ViewInvoiceFragment.notifyDataChanged("Found more rows");
                                    }
                                });

                                //******************************************************************
                                //Save latest orders in the database.
                                //******************************************************************
                                InvoiceDetailsContent.reloadInvoiceDetailsInRoom(invoiceDetailsTemp);

                            } else {
                                //*******************************************************************
                                // Same number of rows so don't bother updating the list.
                                //*******************************************************************
                                Log.d("adline api", "Data has not changed");
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<InvoiceDetails>> call, Throwable t) {

                        //**********************************************************************************
                        // If the api call failed, give a notification to the user.
                        //**********************************************************************************
                        Log.d("Adline api", "api call failed");
                        Log.d("Adline api", t.getMessage());

                        //******************************************************************************************
                        // Using the default shared preferences.  Using the application context - may want to access the
                        // shared prefs from other activities.
                        //******************************************************************************************

                        lengthLastCall = -1; //Indicate couldn't load from api will trigger reload next time
                        loadFromRoomIfPreferred(invoiceDetailViewModel);

                    }
                });

                //***********************************************************************************
                // Sleep so not checking all the time
                //***********************************************************************************
                final int SLEEP_TIME = 10000;
                Log.d("Adline Sleep", "Sleeping for " + (SLEEP_TIME / 1000) + " seconds");
                Thread.sleep(SLEEP_TIME); //Check api every 10 seconds

            } while (true);
        } catch (InterruptedException e) {
            Log.d("Adline api", "Thread interrupted.  Stopping in the thread.");
        }
    }

    /**
     * Check the shared preferences and load from the db if the setting is set to do such a thing.
     * @param invoiceDetailViewModel
     * @since 20220211
     * @author BJM
     */
    public void loadFromRoomIfPreferred(InvoiceDetailViewModel invoiceDetailViewModel) {
        Log.d("Adline room","Check to see if should load from room");
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());
        boolean loadFromRoom = sharedPref.getBoolean(activity.getString(R.string.preference_load_from_room), true);
        Log.d("Adline room","Load from Room="+loadFromRoom);
        if (loadFromRoom) {
            List<InvoiceDetails> testList = MainActivity.getMyAppDatabase().invoicedetailsDAO().selectAllInvoices();
            Log.d("Adline room","Obtained invoice details from the db: "+testList.size());
           invoiceDetailViewModel.setInvoices(testList); //Will add all ticket orders

            //**********************************************************************
            // This method will allow a call to the runOnUiThread which will be allowed
            // to interact with the ui components of the app.
            //**********************************************************************
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d("adline api", "trying to notify adapter that things have changed");
                    ViewInvoiceFragment.notifyDataChanged("Found more rows");
                }

            });

        }
    }


}

