package info.hccis.invoice.entity;

import android.util.Log;

import java.util.List;

import info.hccis.invoice.MainActivity;

public class InvoiceDetailsContent {
    /**
     * This method will take the list passed in and reload the room database
     * based on the items in the list.
     * @param invoiceDetails
     * @since 20220210
     * @author BJM
     */
    public static void reloadInvoiceDetailsInRoom(List<InvoiceDetails> invoiceDetails)
    {
        MainActivity.getMyAppDatabase().invoicedetailsDAO().deleteAll();
        for(InvoiceDetails current : invoiceDetails)
        {
            MainActivity.getMyAppDatabase().invoicedetailsDAO().insert(current);
        }
        Log.d("Adline Room","loading invoice details into Room");
    }


    /**
     * This method will obtain all the ticket orders out of the Room database.
     * @return list of ticket orders
     * @since 20220210
     * @author BJM
     */
    public static List<InvoiceDetails> getInvoiceDetailsFromRoom()
    {
        Log.d("Adline Room","Loading invoice details from Room");

        List<InvoiceDetails> invoiceDetailsBack = MainActivity.getMyAppDatabase().invoicedetailsDAO().selectAllInvoices();
        Log.d("Adline Room","Number of invoices loaded from Room: " + invoiceDetailsBack.size());
        for(InvoiceDetails current : invoiceDetailsBack)
        {
            Log.d("Adline Room",current.toString());
        }
        return invoiceDetailsBack;
    }


}
