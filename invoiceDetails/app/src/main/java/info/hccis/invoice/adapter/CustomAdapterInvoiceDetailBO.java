package info.hccis.invoice.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import info.hccis.invoice.R;



public class CustomAdapterInvoiceDetailBO extends RecyclerView.Adapter<CustomAdapterInvoiceDetailBO.InvoiceDetailBOViewHolder> {

    private List<InvoiceDetails> invoiceDetailsArrayList;
    private int invoiceId;


    public CustomAdapterInvoiceDetailBO(List<InvoiceDetails> invoiceDetailsArrayList) {
        this.invoiceDetailsArrayList = invoiceDetailsArrayList;
    }

    ;

    @NonNull
    @Override
    public InvoiceDetailBOViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_invoices, parent, false);
        return new InvoiceDetailBOViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(@NonNull InvoiceDetailBOViewHolder holder, int position) {


        String id = ""+ invoiceDetailsArrayList.get(position).getId();
        String tutorId = "" + invoiceDetailsArrayList.get(position).getTutorId();
        String startDate = "" + invoiceDetailsArrayList.get(position).getStartDate();
        String endDate = "" + invoiceDetailsArrayList.get(position).getEndDate();
        holder.textViewID.setText(id);
        holder.textViewTutorID.setText(tutorId);
        holder.textViewStartDate.setText(startDate);
        holder.textViewEndDate.setText(endDate);
    }

    @Override
    public int getItemCount() {
        return invoiceDetailsArrayList.size();
    }

    public class InvoiceDetailBOViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewID;
        private TextView textViewTutorID;
        private TextView textViewStartDate;
        private TextView textViewEndDate;

        public InvoiceDetailBOViewHolder(final View view) {
            super(view);
            textViewID= view.findViewById(R.id.textViewIDListItem);
            textViewTutorID = view.findViewById(R.id.textViewTutorIDListItem);
            textViewStartDate = view.findViewById(R.id.textViewStartDateListItem);
            textViewEndDate = view.findViewById(R.id.textViewEndDateListItem);
        }
    }


}
