package info.hccis.invoice;

import android.Manifest;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


import info.hccis.invoice.adapter.CustomAdapterInvoiceDetailBO;
import info.hccis.invoice.databinding.FragmentAddInvoiceBinding;
import info.hccis.invoice.util.ContentProviderUtil;


public class AddInvoiceFragment extends Fragment {
    public static final String KEY = "info.hccis.invoice.ORDER";
    private FragmentAddInvoiceBinding binding;
    private InvoiceDetails invoiceDetails;
    private InvoiceDetailViewModel invoiceDetailViewModel;
    CustomAdapterInvoiceDetailBO.InvoiceDetailBOViewHolder invoiceDetailHolder;
    EditText tutorId;
    EditText endDate,startDate;

    final Calendar myCalendar= Calendar.getInstance();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("AddInvoiceFragment Adline","onCreate triggered");
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        Log.d("AddInvoiceFragment Adline","onCreateView triggered");
        binding = FragmentAddInvoiceBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }



    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("AddInvoiceFragment Adline","onViewCreated triggered");

        invoiceDetailViewModel = new ViewModelProvider(getActivity()).get(InvoiceDetailViewModel.class);
         startDate = binding.editTextstartDate;
        endDate = binding.editTextendDate;

        //************************************************************************************
        // Add a listener on the button.
        //************************************************************************************

        DatePickerDialog.OnDateSetListener sDate =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH,month);
                myCalendar.set(Calendar.DAY_OF_MONTH,day);
                updateStartDate();
            }
        };

        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getActivity(),sDate,myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        DatePickerDialog.OnDateSetListener eDate =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH,month);
                myCalendar.set(Calendar.DAY_OF_MONTH,day);
                updateEndDate();
            }
        };

        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getActivity(),eDate,myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        invoiceDetailViewModel = new ViewModelProvider(getActivity()).get(InvoiceDetailViewModel.class);

        //******************************************************************************
        // Content Providers
        // Check permission for user to use the calendar provider.
        //******************************************************************************

        final int callbackId = 42;
        ContentProviderUtil.checkPermission(getActivity(), callbackId, Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR);

        binding.buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("AddInvoiceFragment Adline", "Calculate was clicked");

                try {
                    //************************************************************************************
                    // Call the calculate method
                    //************************************************************************************

                    calculate();

                    //************************************************************************************
                    // I have made the TicketOrderBO implement Serializable.  This will allow us to
                    // serialize the object and then it can be passed in the bundle.  Note that the
                    // calculate method also sets the
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(KEY, invoiceDetails);

                    //************************************************************************************
                    // See the view model research component.  The ViewModel is associated with the Activity
                    // and the add and view fragments share the same activity.  Am using the view model
                    // to hold the list of TicketOrderBO objects.  This will allow both fragments to
                    // access the list.
                    //************************************************************************************

                    invoiceDetailViewModel.getInvoices().add(invoiceDetails);

                    Snackbar.make(view, "Have added a invoice details.  Want to add this new invoice to the database. " +
                            "How should we handle this?", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                    //Create the event without using Calendar intent
                    long eventID = ContentProviderUtil.createEvent(getActivity(), invoiceDetails.toString());
                    Toast.makeText(getActivity(), "Calendar Event Created (" + eventID + ")", Toast.LENGTH_SHORT);
                    Log.d("Adline Calendar", "Calendar Event Created (" + eventID + ")");


                    //************************************************************************************
                    // Navigate to the view orders fragment.  Note that the navigation is defined in
                    // the nav_graph.xml file.
                    //************************************************************************************

                    NavHostFragment.findNavController(AddInvoiceFragment.this)
                            .navigate(R.id.action_addInvoiceFragment_to_ViewInvoiceFragment, bundle);
                }catch(Exception e){
                    //************************************************************************************
                    // If there was an exception thrown in the calculate method, just put a message
                    // in the Logcat and leave the user on the add fragment.
                    //************************************************************************************
                    Log.d("AddInvoiceFragment Adline", "Error calculating: "+e.getMessage());
                }
            }
        });

    }

    private void updateStartDate() {
        String myFormat="dd-MM-yyyy";
        SimpleDateFormat dateFormat=new SimpleDateFormat(myFormat, Locale.US);
        startDate.setText(dateFormat.format(myCalendar.getTime()));
    }

    private void updateEndDate() {
        String myFormat="dd-MM-yyyy";
        SimpleDateFormat dateFormat=new SimpleDateFormat(myFormat, Locale.US);
        endDate.setText(dateFormat.format(myCalendar.getTime()));
    }

    /**
     * display the details based on the controls on the view.
     * @since 20220118
     * @author CIS2250
     * @throws Exception Throw exception if number of tickets entered caused an issue.
     */
    public void calculate() throws Exception{

        Log.d("Adline-MainActivity", "tutor ID entered =" + binding.editTextTutorID.getText().toString());
        Log.d("Adline-MainActivity", "startDate = " + binding.editTextstartDate.getText().toString());
        Log.d("Adline-MainActivity", "endDate = " + binding.editTextendDate.getText().toString());
        Log.d("Adline-MainActivity", "Save button was clicked.");


       int id =0;

        int tutorId =0;
        tutorId = Integer.parseInt(binding.editTextTutorID.getText().toString());

        String startDate ="";
        startDate = binding.editTextstartDate.getText().toString();

        String endDate ="";
        endDate = binding.editTextendDate.getText().toString();

        invoiceDetails = new InvoiceDetails(tutorId,startDate,endDate);



        //************************************************************************************
        // Add some validation to ensure that the ticket is in a correct range (1-20 in this case.
        //************************************************************************************


    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}