package info.hccis.invoice;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import info.hccis.invoice.adapter.CustomAdapterInvoiceDetailBO;
import info.hccis.invoice.entity.InvoiceDetailViewModel;
import info.hccis.invoice.entity.InvoiceDetails;
import info.hccis.invoice.databinding.FragmentViewInvoiceBinding;
import info.hccis.invoice.util.NotificationApplication;
import info.hccis.invoice.util.NotificationUtil;

public class ViewInvoiceFragment extends Fragment {
    private static Context context;
    private FragmentViewInvoiceBinding binding;
    private List<InvoiceDetails> invoiceDetailBOArrayList;
    private static RecyclerView recyclerView;

    public static RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public static void notifyDataChanged(String message){
        Log.d("adline", "Data changed:  "+message);
        //Send a notification that the data has changed.
        try {
            recyclerView.getAdapter().notifyDataSetChanged();
        }catch(Exception e){
            Log.d("adline api","Exception when trying to notify that the data set as changed");
        }
    }

    /**
     * Provide notification tha the data has changed.  This method will notify the adapter that the
     * rows have changed so it will know to refresh.  It will also send a notification to the user which
     * will allow them to go directly back to the list from another activity of the app.
     * @param message Message to display
     * @param activity - originating activity
     * @param destinationClass - class associated with the intent associated with the notification.
     */
    public static void notifyDataChanged(String message, Activity activity, Class destinationClass){

        Log.d("adline- notification", "Data changed:  "+message);
        try {
            notifyDataChanged(message);
            NotificationApplication.setContext(context);
            NotificationUtil.sendNotification("Invoice App Update", message, activity, MainActivity.class);
        }catch(Exception e){
            Log.d("adline notification", "Exception occured when notifying. "+e.getMessage());
        }



    }
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentViewInvoiceBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        InvoiceDetailViewModel invoiceDetailViewModel = new ViewModelProvider(getActivity()).get(InvoiceDetailViewModel.class);

        Bundle bundle = getArguments();
        InvoiceDetails invoiceDetails = (InvoiceDetails) bundle.getSerializable(AddInvoiceFragment.KEY);
        Log.d("ViewInvoicesFragment Adline", "Invoices passed in:  " + invoiceDetails.toString());

        String output = "";
        for (InvoiceDetails invoices : invoiceDetailViewModel.getInvoices()) {
            output += invoices.toString();
        }

        //************************************************************************************
        // Set the context to be used when sending notifications
        //************************************************************************************

        context = getView().getContext();

        recyclerView = binding.recyclerView;
        invoiceDetailBOArrayList = invoiceDetailViewModel.getInvoices();
        setAdapter();
        binding.buttonAddInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(ViewInvoiceFragment.this)
                        .navigate(R.id.action_ViewInvoiceFragment_to_addInvoiceFragment);
            }
        });
    }

    private void setAdapter() {
        CustomAdapterInvoiceDetailBO adapter = new CustomAdapterInvoiceDetailBO(invoiceDetailBOArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}