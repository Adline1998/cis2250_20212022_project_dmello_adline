package info.hccis.invoice.api;

import java.util.List;

import info.hccis.invoice.entity.InvoiceDetails;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface JsonInvoiceDetailsApi {
    @GET("invoice")
    Call<List<InvoiceDetails>> getInvoices();

    @POST("invoice")
   Call<InvoiceDetails> createInvoice(@Body InvoiceDetails invoiceDetails);
}
