package info.hccis.invoice.entity;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "invoice")
public class InvoiceDetails implements Serializable {

    //Note these match the field names from the database (and keys in json)
    @Ignore
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int tutorId;
    private String startDate;
    private String endDate;

    public InvoiceDetails()
    {

    }
public InvoiceDetails(int id)
{
    this.id = id;
}
    public InvoiceDetails(int tutorId, String startDate, String endDate) {

        this.tutorId = tutorId;
        this.startDate = startDate;
        this.endDate = endDate;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;

    }
    public int getTutorId() {
        return tutorId;
    }

    public void setTutorId(int tutorId) {
        this.tutorId = tutorId;
    }

    public String getStartDate() {
        return startDate;
    }


    public String getEndDate() {
        return endDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Invoice Details" + System.lineSeparator()
                +"Id =  " + id
                +" Tutor Id = " + tutorId +
                ", Start Date = " + startDate +
                ", end Date = " + endDate;
    }
}
