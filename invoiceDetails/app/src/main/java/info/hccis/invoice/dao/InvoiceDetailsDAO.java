package info.hccis.invoice.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import info.hccis.invoice.entity.InvoiceDetails;

@Dao
public interface InvoiceDetailsDAO {

    @Insert
    void insert(InvoiceDetails invoiceDetails);

    @Query("SELECT * FROM invoice")
    List<InvoiceDetails> selectAllInvoices();

    @Query("delete from invoice")
    public void deleteAll();

}