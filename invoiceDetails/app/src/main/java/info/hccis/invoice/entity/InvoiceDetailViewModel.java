package info.hccis.invoice.entity;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

public class InvoiceDetailViewModel extends ViewModel {

    private List<InvoiceDetails> invoiceDetail = new ArrayList();

    public List<InvoiceDetails> getInvoices() {
        return invoiceDetail;
    }

    public void setInvoices(List<InvoiceDetails> invoiceDetail) {
        this.invoiceDetail.clear();
        this.invoiceDetail = invoiceDetail;
    }
}
