package com.example.tennisapp.bo;
/*
Author      : Adline Sandra dmello
Date        : 19 January 2022
course      : cis-2250 Initial App Activity
Description : This is the view model java class used to costCalculate values.
*/
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

public class costCalculateViewModel extends ViewModel {

    private ArrayList<costCalculate> costCalculated = new ArrayList();

    public ArrayList<costCalculate> getCostCalulated() {
        return costCalculated;
    }

    public void setTicketOrders(ArrayList<costCalculate> costCalculated) {
        this.costCalculated = costCalculated;
    }
}