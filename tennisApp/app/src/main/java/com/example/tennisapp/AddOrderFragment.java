package com.example.tennisapp;
/*
Author      : Adline Sandra dmello
Date        : 19 January 2022
course      : cis-2250 Initial App Activity
Description : This is the fragment one(add order)
               The two additional wizards have been implemented:
               They are toggle button and spinner
*/
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.example.tennisapp.bo.costCalculate;
import com.example.tennisapp.bo.costCalculateViewModel;
import com.example.tennisapp.databinding.FragmentAddOrderBinding;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;


public class AddOrderFragment extends Fragment implements AdapterView.OnItemSelectedListener{

    public static final String KEY = "info.hccis.performancehall.ORDER";
    private FragmentAddOrderBinding binding;
    costCalculate costCalculate;
    costCalculateViewModel costCalculateViewModel;
    Spinner spino;
    ToggleButton toggleButton;
    TextView textViewToggle;

    // create array of Strings
    // and store name of courses
    String[] courses = { "C","C#","Java","Php","Android","Artificial Intelligence"};



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("AddOrderFragment adline","onCreate triggered");
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        Log.d("AddOrderFragment BJM","onCreateView triggered");
        binding = FragmentAddOrderBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("AddOrderFragment BJM","onViewCreated triggered");

        costCalculateViewModel = new ViewModelProvider(getActivity()).get(costCalculateViewModel.class);

        //initialising toggleButton and textViewToggle
        toggleButton = binding.toggleButton;
        textViewToggle = binding.textViewToggle;

        toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(toggleButton.isChecked())
                {
                    textViewToggle.setText("Toggle Button set to on");
                }
                else
                {
                    textViewToggle.setText("Toggle Button set to off");
                }

            }
        });
        // Take the instance of Spinner and
        // apply OnItemSelectedListener on it which
        // tells which item of spinner is clicked
        spino = binding.coursesspinner;
        spino.setOnItemSelectedListener(this);

        // Create the instance of ArrayAdapter
        // having the list of courses
        ArrayAdapter ad
                = new ArrayAdapter(
                getActivity(),
                android.R.layout.simple_spinner_item,
                courses);

        // set simple layout resource file
        // for each item of spinner
        ad.setDropDownViewResource(
                android.R.layout
                        .simple_spinner_dropdown_item);

        // Set the ArrayAdapter (ad) data on the
        // Spinner which binds data to spinner
        spino.setAdapter(ad);

        binding.buttonCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("AddOrderFragment BJM", "Calculate was clicked");

                try {
                    calculate();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(KEY, costCalculate);





                    NavHostFragment.findNavController(AddOrderFragment.this)
                            .navigate(R.id.action_AddOrderFragment_to_ViewOrderFragment, bundle);
                }catch(Exception e){
                    Log.d("AddOrderFragment BJM", "Error calculating: "+e.getMessage());
                }
            }
        });

    }
    /**
     * calculate the ticket cost based on the controls on the view.
     * @since 20220118
     * @author CIS2250
     * @throws Exception Throw exception if number of tickets entered caused an issue.
     */
    public void calculate() throws Exception{

        Log.d("adline-MainActivity", "Group size =" + binding.editTextGroupNumber.getText().toString());
        Log.d("adline-MainActivity", "hours  = " + binding.editTextHours.getText().toString());
        Log.d("adline-MainActivity", "member = " + binding.editTextMember.getText().toString());
        Log.d("adline-MainActivity", "Calculate button was clicked.");



        String member="";
        try {
            member= binding.editTextMember.getText().toString();
        }catch(Exception e){
            member="";
        }
        boolean Ismember;
        int groupSize;
        try {
           groupSize = Integer.parseInt(binding.editTextGroupNumber.getText().toString());
        }catch(Exception e){
            groupSize = 0;
        }
        int hours;
        try {
            hours = Integer.parseInt(binding.editTextHours.getText().toString());
        }catch(Exception e){
           hours = 0;
        }
        costCalculate = new costCalculate(groupSize,member,hours);

        try {
            if (!costCalculate.validateGroupSize()) {
                throw new Exception("Invalid Group size");
            }

        }catch(NumberFormatException nfe){
            binding.editTextGroupNumber.setText("");
            binding.textViewDisplay.setText("Invalid Group Size");
            throw nfe;
        }catch(Exception e){
            binding.editTextGroupNumber.setText("");
            binding.textViewDisplay.setText("Maximum Group Size is "+costCalculate.MAX_SIZE);
            throw e;
        }

        try {
            if (!costCalculate.validateHours()) {
                throw new Exception("Invalid Hours");
            }


            costCalculateViewModel.getCostCalulated().add(costCalculate);
            double cost = costCalculate.calculateTotalCost();
            double rate = costCalculate.calculateTotalRate();

            //Now that I have the cost, want to set the value on the textview.
            Locale locale = new Locale("en", "CA");
            DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(locale);
            DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(locale);
            decimalFormat.setDecimalFormatSymbols(dfs);
            String formattedCost = decimalFormat.format(cost);
            binding.textViewDisplay.setText(formattedCost);

        }catch(NumberFormatException nfe){
            binding.editTextHours.setText("");
            binding.textViewDisplay.setText("Invalid Hour");
            throw nfe;
        }catch(Exception e){
            binding.editTextHours.setText("");
            binding.textViewDisplay.setText("The hours must be greater than 0");
            throw e;
        }
}

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        // make toastof name of course
        // which is selected in spinner
        Toast.makeText(getActivity(),
                courses[i],
                Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}