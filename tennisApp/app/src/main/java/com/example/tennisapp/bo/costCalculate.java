package com.example.tennisapp.bo;
/*
Author      : Adline Sandra dmello
Date        : 19 January 2022
course      : cis-2250 Initial App Activity
Description : This is a java class created to calculate the cost
*/
import java.io.Serializable;

/**
 * Business logic class for calculating the cost
 *
 * @author Adline
 * @since 20220118
 */
public class costCalculate implements Serializable {

    //initialisation
    public static final double MAX_SIZE = 4;
    public static final double Private_MEMBER = 55;
    public static final double Private_NON_MEMBER = 60;
    public static final double Size_Two_MEMBER = 30;
    public static final double Size_Two_NON_MEMBER = 33;
    public static final double Size_Three_MEMBER = 21;
    public static final double Size_Three_NON_MEMBER = 23;
    public static final double Size_Four_MEMBER = 16;
    public static final double Size_Four_NON_MEMBER = 18;

    private int groupSize;
    private String member;
    private boolean isMember;
    private int hours;

    //getters and setters
    public int getGroupSize() {
        return groupSize;
    }

    public void setGroupSize(int groupSize) {
        this.groupSize = groupSize;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public boolean isMember() {
        return isMember;
    }

    public void setMember(boolean member) {
        isMember = member;
    }


    /**
     * constructor created
     *
     * @author Adline
     * @since 20220118
     */
    public costCalculate(int groupSize,String member, int hours) {

        this.groupSize = groupSize;
        this.member = member;
        this.setMember(validateIsMember());
        this.hours = hours;
    }

    /**
     *method created to calculate the cost
     * using the if-else method
     *
     * @author Adline
     * @since 20220118
     */
    public double calculateTotalCost() {
        double cost = 0.0;
                if (groupSize == 1 && isMember) {
                    cost = Private_MEMBER * hours;

                }
                else if (groupSize == 1) {
                    cost = Private_NON_MEMBER * hours;

                }

                else if (groupSize == 2 && isMember) {
                    cost = Size_Two_MEMBER * hours;
                }
                else if (groupSize ==2) {
                    cost = Size_Two_NON_MEMBER* hours;
                }
                else if (groupSize == 3 && isMember) {
                    cost = Size_Three_MEMBER * hours;
                }

                else if (groupSize == 3) {
                    cost = Size_Three_NON_MEMBER * hours;
                }
                else if (groupSize == 4 && isMember) {
                    cost = Size_Four_MEMBER * hours;

                } else if(groupSize == 4){
                    cost = Size_Four_NON_MEMBER *hours;

                }
                else
                {
                    cost =0;
                }
          return cost;
    }

    /**
     * method to validate the member
     * using the switch statement as Is a member or not the member
     *
     * @author Adline
     * @since 20220118
     */
    public boolean validateIsMember() {

      boolean valid;

      switch(member)
      {
          case "Y":
          case "y":
              valid = true;
              member = "Y";
              break;
          case "N":
          case "n":
              valid = false;
              member = "N";
              break;
          default:
              valid = false;
              member = "N";
              break;
      }

        return valid;
    }

    /**
     * method to calculate the rate
     * using the if statement based on the membervalidation() method
     *
     * @author Adline
     * @since 20220118
     */
    public  double calculateTotalRate() {
        double rate = 0.0;
        if (groupSize == 1 && isMember) {
            rate = Private_MEMBER;

        }
        else if (groupSize == 1) {
            rate = Private_NON_MEMBER;

        }

        else if (groupSize == 2 && isMember) {
            rate = Size_Two_MEMBER;
        }
        else if (groupSize ==2) {
           rate = Size_Two_NON_MEMBER;
        }
        else if (groupSize == 3 && isMember) {
           rate = Size_Three_MEMBER;
        }

        else if (groupSize == 3) {
            rate = Size_Three_NON_MEMBER;
        }
        else if (groupSize == 4 && isMember) {
            rate = Size_Four_MEMBER;

        } else if(groupSize == 4){
            rate = Size_Four_NON_MEMBER;

        }
        else
        {
            rate =0;
        }
        return rate;
    }

    /**
     * method to validate the groupSize
     *
     * @author Adline
     * @since 20220118
     */
    public boolean validateGroupSize(){
        if(groupSize > 0 && groupSize <= MAX_SIZE){
            return true;
        }else{
            return false;
        }
    }

    /**
     * method to validate the hours so that is not less than 0 or blank
     *
     * @author Adline
     * @since 20220118
     */
    public boolean validateHours(){
        if(hours <= 0){
            return false;
        }else{
            return true;
        }
    }

    /**
     * toString() method to display the values in the second fragment(viewOrderFragment)
     *
     * @author Adline
     * @since 20220118
     */
    @Override
    public String toString() {
        return "Lesson Details" + System.lineSeparator()
                +"Group Size =" + groupSize + "\t" +
                ", Member=" + member + "\t" +
                ", Hours=" + hours + "\t" +
                ", Rate = "+ calculateTotalRate()+ " /hr \t" +
                "cost: $"+ calculateTotalCost();
    }
}

