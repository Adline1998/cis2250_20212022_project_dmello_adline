package com.example.tennisapp;
/*
Author      : Adline Sandra dmello
Date        : 19 January 2022
course      : cis-2250 Initial App Activity
Description : This is fragment two(View Order Fragment) which is
               used to display the values entered in the Fragment one
*/
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.example.tennisapp.bo.costCalculate;
import com.example.tennisapp.bo.costCalculateViewModel;
import com.example.tennisapp.databinding.FragmentViewOrderBinding;


import java.lang.reflect.Array;
import java.util.ArrayList;


public class ViewOrderFragment extends Fragment {


    private FragmentViewOrderBinding binding;

    //replaced by TicketOrderViewModel
    //private static ArrayList<TicketOrderBO> orders = new ArrayList();
    //private static double  total = 0;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentViewOrderBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        costCalculateViewModel costCalculateViewModel = new ViewModelProvider(getActivity()).get(costCalculateViewModel.class);

        //Bundle is accessed to get the ticket order which is passed from the add order fragment.
        Bundle bundle = getArguments();
        costCalculate costCalculateBO = (costCalculate) bundle.getSerializable(AddOrderFragment.KEY);
        Log.d("ViewOrdersFragment Adline", "Cost calculated passed in:  "+costCalculateBO.toString());


        //Build the output to be displayed in the textview
        String output = "";

        // initialising the total cost
        double totalCost = 0;

        // used to calculate the running total of the cost obtained
        // through the calculation
        for (costCalculate order: costCalculateViewModel.getCostCalulated()){
            output += order.toString()+System.lineSeparator();
            totalCost += order.calculateTotalCost();
        }
        output += System.lineSeparator()+"Total Cost: $"+totalCost;

        //The textview which reads the output value and displays it.
        binding.textviewOrderFragment.setText(output);



        //Button sends the user back to the add fragment
        binding.buttonAddOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(ViewOrderFragment.this)
                        .navigate(R.id.action_ViewOrderFragment_to_AddOrderFragment);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}